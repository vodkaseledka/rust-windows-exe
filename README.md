Docker image to produce Windows executable from Rust program. Can be used locally or in pipelines. Source: https://bitbucket.org/vodkaseledka/rust-windows-exe

# Compiling

To compile, run the following command inside your project directory:
```
docker run --rm \
--user "$(id -u)":"$(id -g)" \
-v "$PWD":/app \
-w /app \
-e RUSTC_WRAPPER=/usr/local/cargo/bin/sccache \
rust-windows-exe:latest \
cargo build --release --target x86_64-pc-windows-gnu
```
Note the `RUSTC_WRAPPER` environment variable. Remove this line if you do not want caching.


The resulting file produced to
```
./target/x86_64-pc-windows-gnu/release/
```

# Caches

Cargo cache is located at

```
/usr/local/cargo/registry
```

`sccache` is preinstalled, follow instructions at https://github.com/mozilla/sccache to setup backing storage.

If you want to use local cache, it is referred by `SCCACHE_DIR` environment variable with the default value `/var/cache/sccache`

# (Bash) shell

For Alpine based image no bash installed, use `sh`:
```
docker run --rm -it \
--user "$(id -u)":"$(id -g)" \
-v "$PWD":/app \
-w /app \
-e RUSTC_WRAPPER=/usr/local/cargo/bin/sccache \
rust-windows-exe:1.50-alpine3.12 \
sh
```

To get bash shell in the debian-based image:
```
docker run --rm -it \
--user "$(id -u)":"$(id -g)" \
-v "$PWD":/app \
-w /app \
-e RUSTC_WRAPPER=/usr/local/cargo/bin/sccache \
rust-windows-exe:latest \
bash
```

FROM rust:1.50

ENV SCCACHE_DIR=/var/cache/sccache

RUN apt-get update && \
    apt-get install --no-install-recommends gcc-mingw-w64-x86-64 -y && \
    rm -rf /var/lib/apt/lists/*

RUN set -eux; \
    arch="$(dpkg --print-architecture)"; \
    case "$arch" in \
        amd64) sccacheSha256='e5d03a9aa3b9fac7e490391bbe22d4f42c840d31ef9eaf127a03101930cbb7ca'; \
               sccacheDist='sccache-v0.2.15-x86_64-unknown-linux-musl.tar.gz' ;; \
        arm64) sccacheSha256='90d91d21a767e3f558196dbd52395f6475c08de5c4951a4c8049575fa6894489'; \
               sccacheDist='sccache-v0.2.15-aarch64-unknown-linux-musl.tar.gz' ;; \
        *) echo >&2 "unsupported architecture: $arch"; exit 1 ;; \
    esac; \
    rustup target add x86_64-pc-windows-gnu; \
    rustup toolchain install stable-x86_64-pc-windows-gnu; \
    rm -rf /usr/local/cargo/registry/; \
    sccacheUrl="https://github.com/mozilla/sccache/releases/download/v0.2.15/${sccacheDist}"; \
    wget "$sccacheUrl"; \
    tar xf "$sccacheDist"; \
    cp "${sccacheDist%.*.*}"/sccache /usr/local/cargo/bin/sccache; \
    chmod +x /usr/local/cargo/bin/sccache; \
    rm -rf "${sccacheDist%.*.*}"* ; \
    mkdir -p /var/cache/sccache ; \
    chmod a+w /var/cache/sccache

CMD cargo build --release --target x86_64-pc-windows-gnu
